# Tic-Tac-Toe
This is a simple implementation of a Tic-Tac-Toe game using JavaFX, where a user can play against the computer. The computer's moves are determined by the minimax algorithm, ensuring a challenging opponent for the player.

## Features
- User-friendly graphical interface using JavaFX.
- Single-player mode where the player competes against the computer.
- Intelligent computer opponent using the minimax algorithm for optimal moves.
- Score tracking to keep a record of wins, losses, and ties.

## Requirements
- Java Development Kit (JDK) 17 or later.
- JavaFX 21.

## Getting Started
1. Clone the repository to your local machine:
```bash
git clone https://gitlab.com/assignments7154106/tic-tac-toe.git
```
2. Open this project in an IDE (IntelliJ IDEA recommended).
3. Follow the JavaFX setup guide for your IDE [here](https://openjfx.io/openjfx-docs/).
4. Run the project in your IDE.

## Acknowledgement
This project was inspired by the classic game of Tic-Tac-Toe, the minimax algorithm, and the desire to provide an engaging gaming experience.

## Resources
- JavaFX: https://www.youtube.com/watch?v=9XJicRt_FaI&t=1552s
- Minimax Algorithm: https://www.youtube.com/watch?v=l-hh51ncgDI&t=237s
