package com.example.tictactoe;

public class Game {
    private int turn;
    private int score;
    private final Board board;
    private final Player player;
    private final Computer computer;
    private final Controller controller;

    public Game(Controller controller) {
        this.score = 0;
        this.board = new Board();
        this.player = new Player();
        this.computer = new Computer();
        this.controller = controller;
        this.controller.setBoard(board);    // Passes board instance into controller
    }

    public void handleUserMove() {
        // Evaluate the state
        EvaluationResult eval = board.evaluate();

        // Check for a draw
        if(eval.getValue() == 0) {
            controller.declareDraw();
            return;
        }

        // Check for a win
        if(eval.getValue() == player.getWinningEvaluationValue()) {
            score++;    // Update score state
            controller.updateScore(score);  // Update scoreLabel UI
            controller.displayWinner(player.getSymbol(), eval.getWinningSquareIndices()); // Update UI to display winner
            return;
        }

        // Update turn variable state
        turn = computer.getTurnVariable();

        // Update turn variable in UI
        controller.updateTurn(turn);

        // Fetch a move from the computer
        getComputerMove();
    }

    public void getComputerMove() {
        // Get the best move
        int[] buttonIndices = computer.findBestMove(board.getBoard());

        // Update board state
        board.setBoard(buttonIndices[0], buttonIndices[1], computer.getSymbol());

        // Update UI to reflect the board state
        controller.setBoardUI(buttonIndices[0], buttonIndices[1], computer.getSymbol());

        // Evaluate the state
        EvaluationResult eval = board.evaluate();

        // Check for a draw
        if(eval.getValue() == 0) {
            controller.declareDraw();
            return;
        }

        // Check for a win
        if(eval.getValue() == computer.getWinningEvaluationValue()) {
            score--;    // Update score state
            controller.updateScore(score);  // Update UI to reflect score
            controller.displayWinner(computer.getSymbol(), eval.getWinningSquareIndices());   // Update UI to display winner
            return;
        }

        // Update turn variable state
        turn = player.getTurnVariable();

        // Update turn variable in UI
        controller.updateTurn(turn);
    }

    public void resetGame() {
        board.reset();  // Resets board state
        init(); // Initialize a new game
    }

    public void init() {
        // Randomly assign a turn
        turn = (int)Math.floor(Math.random() * 2);

        if(turn == player.getTurnVariable()) {
            // If its player's turn, update the UI to notify the player it is their turn and wait for them
            // to make a move
            controller.updateTurn(player.getTurnVariable());
        } else {
            // If it is computer's turn, update the UI to notify the player it is computer's turn and find
            // a move for the computer
            controller.updateTurn(computer.getTurnVariable());
            getComputerMove();
        }
    }
}
