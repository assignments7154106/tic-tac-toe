package com.example.tictactoe;

import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.fxml.FXMLLoader;
import javafx.scene.image.Image;
import javafx.application.Application;

public class Main extends Application {
    private static final int WIDTH = 600;
    private static final int HEIGHT = 400;

    @Override
    public void start(Stage stage) throws Exception {
        Controller controller = new Controller();
        FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("hello-view.fxml"));
        fxmlLoader.setController(controller);
        Scene scene = new Scene(fxmlLoader.load(), WIDTH, HEIGHT);

        stage.setTitle("Tic-Tac-Toe");
        stage.getIcons().add(new Image("tic-tac-toe-icon.jpg"));

        stage.setScene(scene);
        stage.show();

        Game game = new Game(controller);   // Passes controller instance into game
        controller.setGame(game);   // Passes the game instance into controller
        game.init();
    }
}
