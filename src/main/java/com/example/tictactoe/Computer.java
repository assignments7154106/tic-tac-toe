package com.example.tictactoe;

public class Computer {
    private static final char SYMBOL = 'O';
    private static final int TURN_VARIABLE = 0;
    private static final int WINNING_EVALUATION_VALUE = -1;

    public char getSymbol() {
        return SYMBOL;
    }
    public int getTurnVariable() {
        return TURN_VARIABLE;
    }
    public int getWinningEvaluationValue() {
        return WINNING_EVALUATION_VALUE;
    }

    private boolean isMovesLeft(char[][] board) {
        for(int i = 0;i < 3;i++) {
            for(int j = 0;j < 3;j++) {
                if(board[i][j] == 'd') {
                    return true;
                }
            }
        }

        return false;
    }

    public int evaluate(char[][] board) {
        // Checking for rows
        for(int i = 0;i < 3;i++) {
            if(board[i][0] == board[i][1] && board[i][1] == board[i][2]) {
                if(board[i][0] == 'X') {
                    return 1;
                } else if(board[i][0] == 'O') {
                    return -1;
                }
            }
        }

        // Checking for columns
        for(int i = 0;i < 3;i++) {
            if(board[0][i] == board[1][i] && board[1][i] == board[2][i]) {
                if(board[0][i] == 'X') {
                    return 1;
                } else if(board[0][i] == 'O') {
                    return -1;
                }
            }
        }

        // Checking for diagonals
        if(board[0][0] == board[1][1] && board[1][1] == board[2][2]) {
            if(board[0][0] == 'X') {
                return 1;
            } else if(board[0][0] == 'O') {
                return -1;
            }
        }
        if(board[0][2] == board[1][1] && board[1][1] == board[2][0]) {
            if(board[0][2] == 'X') {
                return 1;
            } else if(board[0][2] == 'O') {
                return -1;
            }
        }

        // If none of them have won
        return 0;
    }

    private int miniMax(char[][] board, int depth, boolean maximizingPlayer) {
        int eval = evaluate(board);
        // Check for terminating states (Win/Lose/Draw/Max Depth)
        if(eval == -1 || eval == 1) {   // Win or lose state
            return eval;
        }
        if(depth == 0 || !isMovesLeft(board)) { // Draw and max depth reached state
            return eval;
        }

        if(maximizingPlayer) {
            int maxValue = Integer.MIN_VALUE;

            // Check for all children (next possible moves)
            for(int i = 0;i < 3;i++) {
                for(int j = 0;j < 3;j++) {
                    if(board[i][j] == 'd') {
                        // Make a move
                        board[i][j] = 'X';

                        // Find best move
                        maxValue = Math.max(maxValue, miniMax(board, depth - 1, false));

                        // Undo the move to reset the board for next step of recursion
                        board[i][j] = 'd';
                    }
                }
            }

            return maxValue;
        } else {
            int minValue = Integer.MAX_VALUE;

            // Check for all children (next possible moves)
            for(int i = 0;i < 3;i++) {
                for(int j = 0;j < 3;j++) {
                    if(board[i][j] == 'd') {
                        // Make a move
                        board[i][j] = 'O';

                        // Find best move
                        minValue = Math.min(minValue, miniMax(board, depth - 1, true));

                        // // Undo the move to reset the board for next step of recursion
                        board[i][j] = 'd';
                    }
                }
            }

            return minValue;
        }
    }

    public int[] findBestMove(char[][] board) {
        int[] result = new int[]{ -1, -1 };
        int minEval = Integer.MAX_VALUE;

        for(int i = 0;i < 3;i++) {
            for(int j = 0;j < 3;j++) {
                if(board[i][j] == 'd') {
                    // Make a move
                    board[i][j] = 'O';

                    // Evaluate the move
                    int eval = miniMax(board, 5, true);
                    if(eval < minEval) {
                        minEval = eval;
                        result[0] = i;
                        result[1] = j;
                    }

                    // Undo the move to reset the board for next iteration
                    board[i][j] = 'd';
                }
            }
        }

        return result;
    }
}
