package com.example.tictactoe;

public class Player {
    private static final char SYMBOL = 'X';
    private static final int TURN_VARIABLE = 1;
    private static final int WINNING_EVALUATION_VALUE = 1;

    public char getSymbol() {
        return SYMBOL;
    }

    public int getTurnVariable() {
        return TURN_VARIABLE;
    }

    public int getWinningEvaluationValue() {
        return WINNING_EVALUATION_VALUE;
    }
}
