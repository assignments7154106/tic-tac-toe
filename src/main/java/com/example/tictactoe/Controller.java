package com.example.tictactoe;

import javafx.fxml.FXML;
import javafx.event.ActionEvent;
import javafx.scene.control.Label;
import javafx.scene.control.Button;

public class Controller {
    private Game game;    // Add a reference to the Game
    private Board board;    // Add a reference to the Board
    @FXML
    private Button button00;
    @FXML
    private Button button01;
    @FXML
    private Button button02;
    @FXML
    private Button button10;
    @FXML
    private Button button11;
    @FXML
    private Button button12;
    @FXML
    private Button button20;
    @FXML
    private Button button21;
    @FXML
    private Button button22;
    @FXML
    private Label turnLabel;
    @FXML
    private Label scoreLabel;

    public void setBoard(Board board) {
        this.board = board;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    @FXML
    public void updateTurn(int turn) {
        if(turn == 0) {
            turnLabel.setText("Computer's turn");
        } else {
            turnLabel.setText("Your turn");
        }
    }

    @FXML
    public void updateScore(int score) {
        scoreLabel.setText(Integer.toString(score));
    }

    @FXML
    private void changeBackgroundColor(int i, int j) {
        String style = "-fx-background-color: #3D6E3C; -fx-text-fill: white;";

        if(i == 0 && j == 0) {
            button00.setStyle(style);
        } else if(i == 0 && j == 1) {
            button01.setStyle(style);
        } else if(i == 0 && j == 2) {
            button02.setStyle(style);
        } else if(i == 1 && j == 0) {
            button10.setStyle(style);
        } else if (i == 1 && j == 1) {
            button11.setStyle(style);
        } else if(i == 1 && j == 2) {
            button12.setStyle(style);
        } else if(i == 2 && j == 0) {
            button20.setStyle(style);
        } else if(i == 2 && j == 1) {
            button21.setStyle(style);
        } else if(i == 2 && j == 2) {
            button22.setStyle(style);
        }
    }

    @FXML
    public void declareDraw() {
        turnLabel.setText("It's a Draw");
    }

    @FXML
    public void displayWinner(char winner, int[] winningSquareIndices) {
        // 1. Update turn label to display winner
        if(winner == 'X') {
            turnLabel.setText("You Win!");
        } else if(winner == 'O') {
            turnLabel.setText("You Lost!");
        }

        // 2. Set bg color of the winning combination squares to green and color to blue
        for(int i = 0, j = 1;i < 5 && j < 6;i += 2,j += 2) {
            changeBackgroundColor(winningSquareIndices[i], winningSquareIndices[j]);
        }
    }

    @FXML
    public void setBoardUI(int i, int j, char val) {
        String symbol = String.valueOf(val);

        if(i == 0 && j == 0) {
            button00.setText(symbol);
        } else if(i == 0 && j == 1) {
            button01.setText(symbol);
        } else if(i == 0 && j == 2) {
            button02.setText(symbol);
        } else if(i == 1 && j == 0) {
            button10.setText(symbol);
        } else if(i == 1 && j == 1) {
            button11.setText(symbol);
        } else if(i == 1 && j == 2) {
            button12.setText(symbol);
        } else if(i == 2 && j == 0) {
            button20.setText(symbol);
        } else if(i == 2 && j == 1) {
            button21.setText(symbol);
        } else if(i == 2 && j == 2) {
            button22.setText(symbol);
        }
    }

    @FXML
    public void resetBoard() {
        button00.setText("");
        button00.setStyle("-fx-background-color: #B2DEB4;");
        button01.setText("");
        button01.setStyle("-fx-background-color: #B2DEB4;");
        button02.setText("");
        button02.setStyle("-fx-background-color: #B2DEB4;");
        button10.setText("");
        button10.setStyle("-fx-background-color: #B2DEB4;");
        button11.setText("");
        button11.setStyle("-fx-background-color: #B2DEB4;");
        button12.setText("");
        button12.setStyle("-fx-background-color: #B2DEB4;");
        button20.setText("");
        button20.setStyle("-fx-background-color: #B2DEB4;");
        button21.setText("");
        button21.setStyle("-fx-background-color: #B2DEB4;");
        button22.setText("");
        button22.setStyle("-fx-background-color: #B2DEB4;");
    }

    @FXML
    public void selectSquare(ActionEvent event) {
        // Get the clicked button
        Button clickedButton = (Button) event.getSource();
        String buttonId = clickedButton.getId();

        // Check if the square is already selected
        if(!clickedButton.getText().isEmpty()) {
            return;
        }

        // Update UI
        clickedButton.setText("X");

        // Update board state
        int i = Integer.parseInt(buttonId.substring(6, 7));
        int j = Integer.parseInt(buttonId.substring(7, 8));
        board.setBoard(i, j, 'X');

        // Notify the game instance that the user has made a move
        game.handleUserMove();
    }

    @FXML
    public void createNewGame(ActionEvent event) {
        resetBoard();   // Reset board UI
        game.resetGame();   // Reset all game states except score
    }
}
