package com.example.tictactoe;

public class Board {
    private final char[][] board = new char[3][3];

    public Board() {
        // Initialize all values of board to a default value of -1
        for(int i = 0;i < 3;i++) {
            for(int j = 0;j < 3;j++) {
                board[i][j] = 'd';
            }
        }
    }

    public char[][] getBoard() {
        return board;
    }

    public void setBoard(int i, int j, char val) {
        board[i][j] = val;
    }

    // Check for a vertical win
    public EvaluationResult verticalCheck() {
        EvaluationResult result = new EvaluationResult();

        for(int i = 0;i < 3;i++) {
            if(board[i][0] != 'd' && board[i][0] == board[i][1] && board[i][1] == board[i][2]) {
                result.setWinningSquareIndices(new int[] { i, 0, i, 1, i, 2});
                result.setValue(board[i][0] == 'X' ? 1: -1);
                return result;
            }
        }

        return result;
    }

    // Check for a horizontal win
    public EvaluationResult horizontalCheck() {
        EvaluationResult result = new EvaluationResult();

        for(int i = 0;i < 3;i++) {
            if(board[0][i] != 'd' && board[0][i] == board[1][i] && board[1][i] == board[2][i]) {
                result.setWinningSquareIndices(new int[] {0, i, 1, i, 2, i});
                result.setValue(board[0][i] == 'X' ? 1: -1);
                return result;
            }
        }

        return result;
    }

    // Check for a diagonal win
    public EvaluationResult diagonalCheck() {
        EvaluationResult result = new EvaluationResult();

        if(board[0][0] != 'd' && board[0][0] == board[1][1] && board[1][1] == board[2][2]) {
            result.setWinningSquareIndices(new int[] {0, 0, 1, 1, 2, 2});
            result.setValue(board[0][0] == 'X' ? 1: -1);
            return result;
        }

        if(board[0][2] != 'd' && board[0][2] == board[1][1] && board[1][1] == board[2][0]) {
            result.setWinningSquareIndices(new int[] {0, 2, 1, 1, 2, 0});
            result.setValue(board[0][2] == 'X' ? 1 : -1);
            return result;
        }

        return result;
    }

    public EvaluationResult evaluate() {
        EvaluationResult result;

        result = verticalCheck();
        if(result.getValue() != -2) {
            return result;
        }

        result = horizontalCheck();
        if(result.getValue() != -2) {
            return result;
        }

        result = diagonalCheck();
        if(result.getValue() != -2) {
            return result;
        }

        // If there is not a draw
        for(int i = 0;i < 3;i++) {
            for(int j = 0;j < 3;j++) {
                if(board[i][j] == 'd') {
                    return result;
                }
            }
        }

        // If there is a draw
        result.setValue(0);
        result.setWinningSquareIndices(new int[] { -1, -1, -1, -1, -1, -1 });

        return result;
    }

    public void reset() {
        for(int i = 0;i < 3;i++) {
            for(int j = 0;j < 3;j++) {
                board[i][j] = 'd';
            }
        }
    }
}
