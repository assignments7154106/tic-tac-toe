package com.example.tictactoe;

public class EvaluationResult {
    private int value;
    private int[] winningSquareIndices;

    public EvaluationResult() {
        this.value = -2; // evaluation values of -2 refers to a non-terminating position
        this.winningSquareIndices = new int[]{ -1, -1, -1, -1, -1, -1 };
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int[] getWinningSquareIndices() {
        return winningSquareIndices;
    }

    public void setWinningSquareIndices(int[] winningSquareIndices) {
        this.winningSquareIndices = winningSquareIndices;
    }
}
